# -*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function

import os
import shutil

from cookiecutter.main import cookiecutter

# POST HOOK VARS INIT
app_path = os.path.realpath(os.path.curdir)

# django-rest-framework
api_path = os.path.join(app_path, 'api')

# DJANGO REST FRAMEWORK
{% if cookiecutter.__djangorestframework == "n" -%}
# api folder removed if django-rest-framework not selected
shutil.rmtree(api_path)
{% endif -%}

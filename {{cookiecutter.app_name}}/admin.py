# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from . import models
{%- if cookiecutter.__django_image_cropping == "y" %}

from image_cropping import ImageCroppingMixin
{%- endif %}
{%- if cookiecutter.__django_modeltranslation == "y" %}

from modeltranslation.admin import TranslationAdmin
{%- endif %}


class {{cookiecutter.app_class}}Admin(
    {%- if cookiecutter.__django_modeltranslation == "y" -%}TranslationAdmin, {% endif -%}
    {%- if cookiecutter.__django_image_cropping == "y" -%}ImageCroppingMixin, {% endif -%}
    admin.ModelAdmin):
    {%- if cookiecutter.__django_modeltranslation == "y" %}
    class Media:
        # MEDIA FOR MODEL TRANSLATION
        js = (
            'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('modeltranslation/css/tabbed_translation_fields.css',),
        }
    {%- endif %}
    pass

# Register your models here.
admin.site.register(models.{{cookiecutter.app_class}}, {{cookiecutter.app_class}}Admin)

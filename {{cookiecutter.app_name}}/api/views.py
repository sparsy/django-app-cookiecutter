# -*- coding: utf-8 -*-

from rest_framework import generics

from . import serializers

from ..models import {{cookiecutter.app_class}}


# {{cookiecutter.app_class}} LIST
class {{cookiecutter.app_class}}ListView(generics.ListAPIView):
    """
    GET {{cookiecutter.app_class}} list
    """
    model = {{cookiecutter.app_class}}
    serializer_class = serializers.{{cookiecutter.app_class}}ListSerializer

    def get_queryset(self):
        qs = {{cookiecutter.app_class}}.objects.all()
        return qs

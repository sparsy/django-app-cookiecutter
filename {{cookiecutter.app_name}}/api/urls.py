# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views


urlpatterns = [
    # {{cookiecutter.app_name | title}} LIST
    url(
        r'^{{cookiecutter.app_name}}/list/$',
        views.{{cookiecutter.app_class}}ListView.as_view(),
        name='{{cookiecutter.app_name}}_list',
    ),

]

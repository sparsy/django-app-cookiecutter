# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
{%- if cookiecutter.__django_image_cropping == "y" %}

from image_cropping import ImageRatioField
{%- endif %}


@python_2_unicode_compatible
class {{cookiecutter.app_class}}(models.Model):
    """
    {{cookiecutter.app_class}}
    """
    {{cookiecutter.app_name}} = models.CharField(
        max_length=64,
        verbose_name=_('{{cookiecutter.app_title | capitalize}}'),
    )

    {%- if cookiecutter.__django_image_cropping == "y" %}
    image = models.ImageField(
        blank=True, null=True,
        upload_to='images/features',
        verbose_name=_('image'),
    )
    cropping = ImageRatioField(
        'image',
        '476x495'
    )
    {%- endif %}

    class Meta:
        verbose_name = _('{{cookiecutter.app_title}}')
        verbose_name_plural = _('{{cookiecutter.app_title}}s')

    def __str__(self):
        return self.{{cookiecutter.app_name}}

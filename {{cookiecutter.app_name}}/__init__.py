# -*- coding: utf-8 -*-
from __future__ import unicode_literals

{% if cookiecutter.__reusable_app == "yes" -%}
__title__ = '{{cookiecutter.app_title}}'
__version__ = '{{cookiecutter.__version}}'
__author__ = '{{cookiecutter.__author}}'
__license__ = '{{cookiecutter.__license}}'
__copyright__ = 'Copyright {{cookiecutter.__year}} {{cookiecutter.__author}}'

# Version synonym
VERSION = __version__

{% endif -%}

default_app_config = '{{cookiecutter.absolute_import_path}}.apps.{{cookiecutter.app_class}}Config'

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class {{cookiecutter.app_class}}Config(AppConfig):
    name = '{{cookiecutter.absolute_import_path}}'
    label = '{{cookiecutter.app_name}}'
    verbose_name = _('{{cookiecutter.app_title}}')

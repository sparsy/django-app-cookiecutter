# -*- coding: utf-8 -*-

{% if cookiecutter.__django_image_cropping == "y" -%}
from easy_thumbnails.files import get_thumbnailer
{%- endif %}
from rest_framework import serializers

from ..models import {{cookiecutter.app_class}}


class {{cookiecutter.app_class}}ListSerializer(serializers.ModelSerializer):
    {% if cookiecutter.__django_image_cropping == "y" -%}
    cropped_image = serializers.SerializerMethodField()

    {% endif -%}

    class Meta:
        model = {{cookiecutter.app_class}}
        {% if cookiecutter.__django_image_cropping == "y" -%}
        fields = ('id', '{{cookiecutter.app_name}}', 'cropped_image',)
        {%- else %}
        fields = ('id', '{{cookiecutter.app_name}}',)
        {%- endif %}

    {% if cookiecutter.__django_image_cropping == "y" -%}
    def get_cropped_image(self, obj):
        request = self.context.get('request')
        crop_url = get_thumbnailer(obj.image).get_thumbnail({
                'size': (476, 495),
                'box': obj.cropping,
                'crop': True,
                'detail': True,
            }).url
        return request.build_absolute_uri(crop_url)
    {%- endif %}

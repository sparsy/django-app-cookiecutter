# -*- coding: utf-8 -*-
import sys
absolute_import_path = '{{ cookiecutter.absolute_import_path }}'
app_name = '{{ cookiecutter.app_name }}'

if not absolute_import_path.endswith(app_name):
    print("ERROR: Can't import '{app} config from '{path}'!".format(
        app=app_name, path=absolute_import_path)
    )

    # exits with status 1 to indicate failure
    sys.exit(1)
